//Question 4 using do while loop
#include <stdio.h>
int main ()
{
	int N;
	int f=1;
	printf("Enter a positive integer to find out its factors\n");
	printf("Number: ");
	scanf("%d",&N);
	printf("Factors of %d are\n",N);
	do
	{
		if (N%f==0)
			printf("%d\n",f);
		f++;
	}
	while (N>=f);
	return 0;
}
