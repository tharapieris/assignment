//Question 2 using for loop
#include <stdio.h>
int main ()
{
	int N;
	int f=2;
	printf("Enter a positive integer to know whether it is a prime number or not !\n");
	printf("Enter the number\n");
	scanf("%d",&N);
	for (f=2;N>=f;f++)
	{
		if (N%f==0)
		{
			break;
		}
	}
	if (N==f)
		printf("%d is a prime number",N);
	else
		printf("%dis not a prime number",N);
	return 0;
}
