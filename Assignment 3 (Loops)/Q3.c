//Question 3 using while loop
#include <stdio.h>
int main ()
{
	int N,x,y;
	printf("Enter a positive integer to get it reversed !\n");
	printf("Your number: ");
	scanf("%d",&N);
	printf("Reversed number is: ");
	while (N>=10)
	{
		x=N/10;
		y=N%10;
		printf("%d",y);
		N=x;
		if(x<10)
		{
			break;
		}
	}
	printf("%d",x);
	return 0;
}

