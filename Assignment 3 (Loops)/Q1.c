//Question 1 using while loop
#include <stdio.h>
int main ()
{
	int N,sum;
	printf("Enter positive integers other than 0 to get the total\n");
	printf("Enter the number: \n ");
	scanf("%d",&N);
	while (N>0)
	{
		sum+=N;
		printf("Enter the number: \n");
		scanf("%d",&N);
	}
	printf("The total of the positive numbers is = %d",sum);
	return 0;
}

